#include "libdebug.h"
#include "libencrypt.h"

#include <string.h>

//int busted_strcmp(unsigned char * a, unsigned char * b);

int main(void)
{
    openssl_init();             // XXX: may not be needed


    unsigned char * key = (unsigned char *) "00000000000000000000000000000000";
    unsigned char * iv  = (unsigned char *) "0000000000000000";

    unsigned char * plaintext = (unsigned char *) "Hello World!";
    int plaintext_size = 13;
    P_INFO("Plaintext: %s", plaintext);

    unsigned char * encrypted = malloc(plaintext_size);
    if(!encrypted){
        P_ERR_STR("malloc() failed");
        return EXIT_FAILURE;
    }
    unsigned char * decrypted = malloc(plaintext_size);
  if(!decrypted){
      P_ERR_STR("malloc() failed");
      return EXIT_FAILURE;
  }

    int encrypted_size = encrypt_chacha20(plaintext, plaintext_size, key, iv, encrypted);
    if (encrypted_size == -1) {
        P_ERR_STR("encrypt_chacha20 failed, exiting!");
        return EXIT_FAILURE;
    }
    P_INFO("Encrypted: %s", encrypted);
    
    int decrypted_size = decrypt_chacha20(encrypted, encrypted_size, key, iv, decrypted);
    if (decrypted_size == -1) {
        P_ERR_STR("decrypt_chacha20 failed, exiting!");
        return EXIT_FAILURE;
    }
    P_INFO("Decrypted: %s", decrypted);

    /*
    // fail if plaintext is not the same as the decrypted text
    if (busted_strcmp(plaintext, plaintext_size, decrypted, decrypted_size) == 0) {
        P_INFO_STR("Plaintext and decrypted text match.  Test success!");

        return 0;
    } else {
        P_ERR_STR("Plaintext and decrypted text do not match.  Test failed!");

        return 1;
    }
    */
    

    openssl_cleanup();          // XXX: may not be needed

    (void) decrypted_size;      // XXX: get rid of compiler errors

    return EXIT_SUCCESS;
}

/*
int busted_strcmp(unsigned char * a, int a_size, unsigned char * b, int b_size) {
    if (a_size != b_size) {
        return 0;
    }

    char * curr_a = a;
    char * curr_b = b;

    int a = a_size
    while () {
        if (curr_a != curr_b) {
            return 0;
        }

        curr_a++;        
        curr_b++;
    }

    if (curr_a == '\0' && curr_b == '\0') {
        return 1;
    }

    return 0;
}
*/
