#ifndef LIB_CRYPT_H_
#define LIB_CRYPT_H_

#include "openssl/conf.h"
#include "openssl/evp.h"
#include "openssl/err.h"

#include "libdebug.h"

int openssl_init(void);
int openssl_cleanup(void);

/* chacha20 details:
 * Key length is 256 bits.  IV is 128 bits long.
 * first 32 bits are a counter (in little endian byte order)
 * the counter is followed by a 96 bit nonce
 */
int encrypt_chacha20(const unsigned char * input, int input_size, unsigned char * key, unsigned char * iv, unsigned char * output);

int decrypt_chacha20(const unsigned char * input, int input_size, unsigned char * key, unsigned char * iv, unsigned char * output);

#endif

