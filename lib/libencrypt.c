#include "libencrypt.h"

int openssl_init(void) {

// according to https://stackoverflow.com/a/54558536, if we're using openssl >= 1.1
// we no longer need startup or cleanup code since it's done automatically.
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    // initialize libcrypto
    // see https://wiki.openssl.org/index.php/Libcrypto_API

    ERR_load_crypto_strings();

    // links all algos, TODO: change to just add the ciphers/digests we need
    OpenSSL_add_all_algorithms();

    // configure openssl using the default section, openssl_conf
    // OPENSSL_config is deprecated, instead using CONF_modules_load_file()
    CONF_modules_load_file(NULL, NULL, CONF_MFLAGS_DEFAULT_SECTION);
#endif
    return 0;
}

int openssl_cleanup(void) {
#if OPENSSL_VERSION_NUMBER < 0x10100000L
    // clean up after libcrypto // see https://wiki.openssl.org/index.php/Libcrypto_API
    // see https://en.wikibooks.org/wiki/OpenSSL/Initialization
    // not exactly sure what calls are necessary - TODO: check for leaks and such

    ERR_free_strings();

    EVP_cleanup();

    CRYPTO_cleanup_all_ex_data();

    CONF_modules_free();

#endif
    return 0;
}

int encrypt_chacha20(const unsigned char * input, int input_size, unsigned char * key, unsigned char * iv, unsigned char * output) {

    // see: https://www.openssl.org/docs/man1.1.0/man3/EVP_chacha20.html

    int output_size = 0;
    int temp_size = 0;

    EVP_CIPHER_CTX * ctx;
    
    // create and initialize EVP cipher context
    if ((ctx = EVP_CIPHER_CTX_new()) == NULL) {
        P_ERR("Failed to create cipher context! -- %s", ERR_error_string(ERR_get_error(), NULL));  

        return -1;
    }

    if (EVP_EncryptInit_ex(ctx, EVP_chacha20(), NULL, key, iv) != 1) {
        P_ERR("Failed to set up cipher context for encryption! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }

    
    if (EVP_EncryptUpdate(ctx, output, &output_size, input, input_size) != 1) {
        P_ERR("Failed to encrypt the input! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }
    output_size += temp_size;

    if (EVP_EncryptFinal_ex(ctx, output + output_size, &temp_size) != 1) {
        P_ERR("Failed to finalize encryption! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }
    output_size += temp_size;

    
    // finished using the cipher
    EVP_CIPHER_CTX_free(ctx);

    return output_size;
}

int decrypt_chacha20(const unsigned char * input, int input_size, unsigned char * key, unsigned char * iv, unsigned char * output) {

    // see: https://www.openssl.org/docs/man1.1.0/man3/EVP_chacha20.html

    int output_size = 0;
    int temp_size = 0;

    EVP_CIPHER_CTX * ctx;
    
    // create and initialize EVP cipher context
    if ((ctx = EVP_CIPHER_CTX_new()) == NULL) {
        P_ERR("Failed to create cipher context! -- %s", ERR_error_string(ERR_get_error(), NULL));  

        return -1;
    }

    if (EVP_DecryptInit_ex(ctx, EVP_chacha20(), NULL, key, iv) != 1) {
        P_ERR("Failed to set up cipher context for decryption! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }

    
    if (EVP_DecryptUpdate(ctx, output, &output_size, input, input_size) != 1) {
        P_ERR("Failed to decrypt the input! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }
    output_size += temp_size;

    if (EVP_DecryptFinal_ex(ctx, output + output_size, &temp_size) != 1) {
        P_ERR("Failed to finalize decryption! -- %s", ERR_error_string(ERR_get_error(), NULL));

        return -1;
    }
    output_size += temp_size;

    
    // finished using the cipher
    EVP_CIPHER_CTX_free(ctx);

    return output_size;
}
